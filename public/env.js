window.env = {
  "MANPATH": "/opt/homebrew/share/man::",
  "TERM_PROGRAM": "iTerm.app",
  "NODE": "/opt/homebrew/Cellar/node/20.0.0/bin/node",
  "INIT_CWD": "/Users/karenarana/Documents/Cursos/React/e-commerce",
  "_P9K_TTY": "/dev/ttys000",
  "TERM": "xterm-256color",
  "SHELL": "/bin/zsh",
  "npm_config_metrics_registry": "https://registry.npmjs.org/",
  "HOMEBREW_REPOSITORY": "/opt/homebrew",
  "TMPDIR": "/var/folders/wp/cs6w787124d_06j911hln47m0000gn/T/",
  "npm_config_global_prefix": "/opt/homebrew",
  "TERM_PROGRAM_VERSION": "3.4.19",
  "COLOR": "1",
  "TERM_SESSION_ID": "w0t0p0:79A4A660-8DE1-41F7-8885-76A9692B116F",
  "npm_config_noproxy": "",
  "SDKMAN_PLATFORM": "darwinarm64",
  "npm_config_local_prefix": "/Users/karenarana/Documents/Cursos/React/e-commerce",
  "ZSH": "/Users/karenarana/.oh-my-zsh",
  "USER": "karenarana",
  "LS_COLORS": "di=1;36:ln=35:so=32:pi=33:ex=31:bd=34;46:cd=34;43:su=30;41:sg=30;46:tw=30;42:ow=30;43",
  "COMMAND_MODE": "unix2003",
  "npm_config_globalconfig": "/opt/homebrew/etc/npmrc",
  "SDKMAN_CANDIDATES_API": "https://api.sdkman.io/2",
  "SSH_AUTH_SOCK": "/private/tmp/com.apple.launchd.g6AAxblCp2/Listeners",
  "__CF_USER_TEXT_ENCODING": "0x1F5:0x0:0x0",
  "npm_execpath": "/opt/homebrew/lib/node_modules/npm/bin/npm-cli.js",
  "PAGER": "less",
  "LSCOLORS": "Gxfxcxdxbxegedabagacad",
  "PATH": "/Users/karenarana/Documents/Cursos/React/e-commerce/node_modules/.bin:/Users/karenarana/Documents/Cursos/React/node_modules/.bin:/Users/karenarana/Documents/Cursos/node_modules/.bin:/Users/karenarana/Documents/node_modules/.bin:/Users/karenarana/node_modules/.bin:/Users/node_modules/.bin:/node_modules/.bin:/opt/homebrew/lib/node_modules/npm/node_modules/@npmcli/run-script/lib/node-gyp-bin:/Users/karenarana/.sdkman/candidates/java/current/bin:/opt/homebrew/bin:/opt/homebrew/sbin:/usr/local/bin:/System/Cryptexes/App/usr/bin:/usr/bin:/bin:/usr/sbin:/sbin:/var/run/com.apple.security.cryptexd/codex.system/bootstrap/usr/local/bin:/var/run/com.apple.security.cryptexd/codex.system/bootstrap/usr/bin:/var/run/com.apple.security.cryptexd/codex.system/bootstrap/usr/appleinternal/bin",
  "npm_package_json": "/Users/karenarana/Documents/Cursos/React/e-commerce/package.json",
  "_": "/Users/karenarana/Documents/Cursos/React/e-commerce/node_modules/.bin/react-dotenv",
  "npm_config_userconfig": "/Users/karenarana/.npmrc",
  "npm_config_init_module": "/Users/karenarana/.npm-init.js",
  "__CFBundleIdentifier": "com.googlecode.iterm2",
  "npm_command": "start",
  "PWD": "/Users/karenarana/Documents/Cursos/React/e-commerce",
  "JAVA_HOME": "/Users/karenarana/.sdkman/candidates/java/current",
  "npm_lifecycle_event": "start",
  "EDITOR": "vi",
  "P9K_SSH": "0",
  "npm_package_name": "reto-3-e-commerce-karen",
  "P9K_TTY": "old",
  "ITERM_PROFILE": "Default",
  "XPC_FLAGS": "0x0",
  "npm_config_node_gyp": "/opt/homebrew/lib/node_modules/npm/node_modules/node-gyp/bin/node-gyp.js",
  "npm_package_version": "0.1.0",
  "XPC_SERVICE_NAME": "0",
  "SHLVL": "2",
  "HOME": "/Users/karenarana",
  "COLORFGBG": "7;0",
  "LC_TERMINAL_VERSION": "3.4.19",
  "HOMEBREW_PREFIX": "/opt/homebrew",
  "ITERM_SESSION_ID": "w0t0p0:79A4A660-8DE1-41F7-8885-76A9692B116F",
  "npm_config_cache": "/Users/karenarana/.npm",
  "LESS": "-R",
  "LOGNAME": "karenarana",
  "npm_lifecycle_script": "react-dotenv && react-scripts start",
  "SDKMAN_DIR": "/Users/karenarana/.sdkman",
  "LC_CTYPE": "UTF-8",
  "npm_config_user_agent": "npm/9.6.4 node/v20.0.0 darwin arm64 workspaces/false",
  "SDKMAN_CANDIDATES_DIR": "/Users/karenarana/.sdkman/candidates",
  "INFOPATH": "/opt/homebrew/share/info:",
  "HOMEBREW_CELLAR": "/opt/homebrew/Cellar",
  "_P9K_SSH_TTY": "/dev/ttys000",
  "LC_TERMINAL": "iTerm2",
  "npm_node_execpath": "/opt/homebrew/Cellar/node/20.0.0/bin/node",
  "npm_config_prefix": "/opt/homebrew",
  "COLORTERM": "truecolor",
  "FIREBASE_API_KEY": "AIzaSyDWs5ZfzE5pUnXyxxEKVJgMCzzJ6jD4yI4",
  "FIREBASE_AUTH_DOMAIN": "reto-3-e-commerce-karen.firebaseapp.com",
  "FIREBASE_PROJECT_ID": "reto-3-e-commerce-karen",
  "FIREBASE_STORAGE_BUCKET": "reto-3-e-commerce-karen.appspot.com",
  "FIREBASE_MESSAGING_SENDER_ID": "272160416128",
  "FIREBASE_APP_ID": "1:272160416128:web:13d8d4f96adafb3aea2165",
  "FIREBASE_MEASUREMENT_ID": "G-WH7Q8JP006"
};