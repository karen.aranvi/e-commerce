import { useSelector, useDispatch } from "react-redux";
import { useAuth } from "../context/AuthContext";
import { useEffect } from "react";
import { getAllProducts } from "../actions/products";

export default function Home() {

	const { productList } = useSelector(state => ({ productList: state.products.productList }))

	const dispatch = useDispatch()

	useEffect(() => {
		dispatch(getAllProducts())
	}, [])

	useEffect(() => {
		console.log('product list', productList)
	}, [productList])

	return (
		<>
			<h1>Hola desde home</h1>
		</>
	);
}
